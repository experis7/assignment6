import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap,tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../consts/storage-keys.enum';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

const {apiTrainers, apiKey} = environment;
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

//Model
public login(trainername:string): Observable<Trainer>{
  return this.checkTrainerName(trainername)
    .pipe(
      switchMap((trainer:Trainer | undefined) => {
        if(trainer === undefined){
          return this.createTrainer(trainername);
        }
        return of(trainer);
      }),
      tap((trainer:Trainer)=>{
        StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer)
      })
    )
}
//Login


//Check if user exists
private checkTrainerName(trainername:string): Observable<Trainer|undefined>{
  return this.http.get<Trainer[]>(`${apiTrainers}?trainername=${trainername}`)
    .pipe(
      map((response: Trainer[])=>response.pop())
    )
}
// If the user does not exist- Create a new user
private createTrainer(trainername:string): Observable<Trainer>{
  const trainer = {
    trainername,
    pokemon: []
  };
  
  const headers = new HttpHeaders({
    "Content-Type": "application/json",
    "X-API-Key": apiKey
  });

  return this.http.post<Trainer>(apiTrainers, trainer,{
    headers
  })
}
// If the user exists or it is successfully created, store the user






}
