import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {  Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';

const{apiKey, apiTrainers} = environment
@Injectable({
  providedIn: 'root'
})
export class TrainersPokemonsService {

  public loading: boolean = false
  constructor(
    private http: HttpClient,
    private readonly pokemonService:PokemonCatalogueService, 
    private readonly trainerService: TrainerService
  ) { }


  public addPokemon(name:string):Observable<Trainer>{
    if(!this.trainerService.trainer){
      throw new Error("addPokemon: There is no user");
    }
    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonService.pokemonByName(name);
    if(!pokemon){
      throw new Error("addPokemon: No pokemon named:"+ name);
    }

    if(this.trainerService.isCaught(name)){
      this.trainerService.removeFromCollection(name);
    }else{
      this.trainerService.addToCollection(pokemon)
    }
    
    const headers = new HttpHeaders({
      'content-type' : 'application/json',
      "X-API-Key": apiKey

    });


    return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`,{
      pokemon: [...trainer.pokemon]
    },{
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer)=>{
        this.trainerService.trainer = updatedTrainer;
      })
    )
  }
}
