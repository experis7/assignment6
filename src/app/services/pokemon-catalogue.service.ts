import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, Pokemons } from '../models/pokemon.model';  
const {apiPokemon} = environment
@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  constructor(private readonly http:HttpClient) { }
  private _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading = false;

  get pokemons(): Pokemon[]{
    return this._pokemons!;
  }

  get error(): string{
    return this._error;
  }
  get loading(): boolean{
    return this._loading;
  }
  
  public findFirstGenPokemon():void{

    if(this._pokemons.length > 0 || this.loading){
      return;
    }
    this.http.get<Pokemons>(apiPokemon)
      .pipe(
        finalize(() =>{
          this._loading = false;
        })
      )
      .subscribe( {
        next: (pokemons: Pokemons) => {
          this._pokemons = pokemons.results;
          console.log(pokemons);
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })
  }

  public pokemonByName(name: string): Pokemon|undefined{
    return this._pokemons?.find((pokemon:Pokemon) => pokemon.name === name)
  }
}
