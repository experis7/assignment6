export interface PokemonDetails{
    id: number;
    name: string;
    is_default: boolean;
    base_experience: number;
    height: number;
    location_area_encounters: string;
    order: number;
    weight: number;
    abilities: PokemonAbilities[];
    forms: PokemonForms[];
    game_indices: PokemonGameIndices[];
    moves: PokemonMoves[];
    species: PokemonSpecies;
    sprites: PokemonSprites;
    stats: PokemonStats[];
    types: PokemonType[];


}

export interface PokemonAbilities{
    ability: PokemonAbility;
    is_hidden:boolean;
    slot: number;
}

export interface PokemonAbility{
    name: string;
    url:string; 
}
export interface PokemonForms{
    name: string;
    url:string;
}

export interface PokemonGameIndices{
    game_index: number;
    url: string;
}
export interface PokemonGameIndicesVersion{
    name:string;
    url:string;
}

export interface PokemonMoves{
    move: PokemonMove;
    version_group_details: PokemonMovesVersionGroupDetails[]
}

export interface PokemonMove{
    name:string;
    url:string;
}
export interface PokemonMovesVersionGroupDetails{
    level_learned_at : number;
    move_learn_method: PokemonMovesVersionGroupDetailsMoveLearnMethod;
    version_group: PokemonMovesVersionGroupDetailsVersionGroup,
}

export interface PokemonMovesVersionGroupDetailsMoveLearnMethod{
    name: string;
    url: string;
}

export interface PokemonMovesVersionGroupDetailsVersionGroup{
    name:string;
    url:string;
}

export interface PokemonSpecies{
    name:string;
    url:string;
}
export interface PokemonSprites{
    back_default: string;
    back_female: string;
    back_shiny: string;
    back_shiny_female: string;
    front_default: string;
    front_female:string;
    front_shiny: string;
    front_shiny_female:string;
}

export interface PokemonStats{
    base_stat: number;
    effort: number;
}

export interface PokemonType{
    name: string;
    url: string;
}