import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Trainer } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
  @Output() login: EventEmitter<void> = new EventEmitter();
  constructor(
    private readonly trainerService:TrainerService,
    private readonly loginservice:LoginService
    ){}

  public loginSubmit(loginForm: NgForm):void{
    const {trainername} = loginForm.value
    this.loginservice.login(trainername)
      .subscribe({
        next: (trainer:Trainer) => {
            console.log(trainer.trainername)
            this.trainerService.trainer = trainer;
            this.login.emit();
        },
        error: () => {
            
        }
      })
  }
}
