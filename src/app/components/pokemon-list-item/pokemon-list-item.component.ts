import { Component, Host, Input, ViewEncapsulation } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PokemonListItemComponent {
 @Input() pokemon?: Pokemon;

}
