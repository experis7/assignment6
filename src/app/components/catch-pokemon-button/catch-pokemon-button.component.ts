import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { TrainersPokemonsService } from 'src/app/services/trainers-pokemons.service';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.css']
})
export class CatchPokemonButtonComponent implements OnInit {
  public loading: boolean = false
  public isCaught = false;
  @Input() pokemonName: string = "";
 
 constructor(
  private readonly trainersPokemonService: TrainersPokemonsService,
  private trainerService: TrainerService
  ){}

  ngOnInit(): void {
      this.isCaught = this.trainerService.isCaught(this.pokemonName);
  }

  onCatchClick():void{
    this.loading = true;
    //Add pokemon to trainers collection
    this.trainersPokemonService.addPokemon(this.pokemonName)
      .subscribe({
        next:(trainer:Trainer) => {
          this.loading = false;
          this.isCaught = this.trainerService.isCaught(this.pokemonName);
        },
        error:(error: HttpErrorResponse)=>{
          console.log("Error", error.message);
        }
      })
    
  }
}
